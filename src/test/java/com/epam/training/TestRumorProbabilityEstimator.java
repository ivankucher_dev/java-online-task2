package com.epam.training;

public class TestRumorProbabilityEstimator extends RumorProbabilityEstimator implements ProbabilityEstimator {
    TestRumorProbabilityEstimator(int N) throws NumberOfGuestsException {
        super(N);
    }

    TestRumorProbabilityEstimator(int N, int attempts) throws NumberOfGuestsException {
        super(N, attempts);
    }

    public void calculateProbability() {
        super.calculateProbability();
    }

}
