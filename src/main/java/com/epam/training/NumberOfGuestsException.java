package com.epam.training;

/**
 * NumberOfGuestsException exception class
 * Main function is to protect {@link RumorProbabilityEstimator}
 * against incorrect input of number of guests
 *
 * @author Ivan Kucher
 * @version 1.0
 * @see RumorProbabilityEstimator
 * @since 2019-07-30
 */
public class NumberOfGuestsException extends Exception {

    /**
     * Constructor to call super from Exception
     *
     * @param message text to show if exception called
     */
    NumberOfGuestsException(String message) {
        super(message);
    }
}
