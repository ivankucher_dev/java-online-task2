package com.epam.training;

import java.util.ArrayList;

/**
 * Main class.
 * The main class from which we start our program.
 *
 * @author Ivan Kucher
 * @version 1.0
 * @since 2019-07-30
 */
public class Main {

    /**
     * Main function to start our program , initialize {@link RumorProbabilityEstimator}
     *
     * @param args arguments for main function
     */
    public static void main(String[] args) {


        RumorProbabilityEstimator aliceParty = null;
        try {
            aliceParty = new RumorProbabilityEstimator(7, 100);
        } catch (NumberOfGuestsException e) {
            e.printStackTrace();
        }
        aliceParty.calculateProbability();
        aliceParty.showAvarageNumberOfPeopleWhoHeared();


    }
}
