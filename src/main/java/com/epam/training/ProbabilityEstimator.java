package com.epam.training;

import java.util.ArrayList;

/**
 * ProbabilityEstimator interface using for {@link com.epam.training.RumorProbabilityEstimator}
 * to implements simple methods
 *
 * @author Ivan Kucher
 * @version 1.0
 * @since 2019-07-30
 */
public interface ProbabilityEstimator {

    /**
     * Method will be used for showing final results
     */
    void showResult();

    /**
     * This method will be used to calculate probability
     * that everyone of guests will hear the rumor
     */
    void calculateProbability();

    void showAvarageNumberOfPeopleWhoHeared();

    void increaseNumberOfPeopleWhoHeared();

    /**
     * This method will be used to analyze list of guests.
     *
     * @param guests list of guests who have already heared the rumor
     * @return true if the guests contains all guests
     */
    boolean isAllHeared(ArrayList<Integer> guests);

}
