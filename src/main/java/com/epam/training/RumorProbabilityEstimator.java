package com.epam.training;

import java.util.ArrayList;

/**
 * RumorProbabilityEstimator class implements {@link ProbabilityEstimator}.
 * The main functions is to estimate the probability that
 * everyone at the party will hear the rumor before it stops
 * propagating ,and also calculate an estimate of the expected number of people to hear
 * the rumor.
 *
 * @author Ivan Kucher
 * @version 1.0
 * @since 2019-07-30
 */
public class RumorProbabilityEstimator implements ProbabilityEstimator {

    /**
     * Number of guests at the party
     */
    private int n;

    private int attempts;
    /**
     * The public probability that everyone will hear the rumor
     */
    private float probability;
    /**
     * Number of people from all attempts
     */
    private int numberOfPeopleFromAllAttempts;
    /**
     * Avarage number of guests who heared the rumor
     */
    private double averageNumberWhoHeared;
    /**
     * Counter of successfull attempt , everyone at the party heared the rumor
     */
    private int successfullAttemptsCounter;

    /**
     * Creates a party with the specified number of guests.
     *
     * @param N Number of guests at the party .
     * @throws NumberOfGuestsException if number of guests less than 2.
     */
    RumorProbabilityEstimator(int N) throws NumberOfGuestsException {
        this.attempts = 50;
        this.n = N;
        if (N <= 2) {
            throw new NumberOfGuestsException("N must be greater than 2");
        }
    }

    /**
     * Creates a party with the specified number of guests and specified number of attempts
     *
     * @param N        Number of guests at the party .
     * @param attempts Number of attempts for accuracy of probability.
     * @throws NumberOfGuestsException if number of guests less than 2.
     */
    RumorProbabilityEstimator(int N, int attempts) throws NumberOfGuestsException {
        this.n = N;
        this.attempts = attempts;
        if (N <= 2) {
            throw new NumberOfGuestsException("N must be greater than 2");
        }
    }


    /**
     * Calculate probability that everyone at the party will hear the rumor
     *
     * @since 1.0
     */
    public void calculateProbability() {
        ArrayList<Integer> alreadyHeared = new ArrayList<Integer>();
        int currentPerson;
        int previousPerson;
        int randomGuest;
        boolean allHeared;


        for (int i = 0; i < attempts; i++) {
            allHeared = false;
            currentPerson = 0;
            previousPerson = 0;
            alreadyHeared.clear();
            alreadyHeared.add(0);
            System.out.println("\n");
            while (!allHeared) {

                randomGuest = Random.getRandom(this.n, previousPerson, currentPerson);
                System.out.print("random = [" + randomGuest + "] current = [" + currentPerson + "] ");
                if (!alreadyHeared.contains(randomGuest)) {
                    previousPerson = currentPerson;
                    currentPerson = randomGuest;
                    alreadyHeared.add(randomGuest);
                    increaseNumberOfPeopleWhoHeared();

                } else {

                    if (isAllHeared(alreadyHeared)) {
                        this.successfullAttemptsCounter += 1;
                        allHeared = true;
                    } else {
                        allHeared = true;
                    }

                }

            }

        }
        probability = (float) this.successfullAttemptsCounter / this.attempts;
        this.averageNumberWhoHeared = (double) this.numberOfPeopleFromAllAttempts / this.attempts;

        showResult();

    }

    /**
     * Method show final results : number of attempts , probability
     *
     * @since 1.0
     */
    public void showResult() {
        System.out.println("Number of attempts = " + this.attempts);
        System.out.println("Probability that everyone will hear the rumor = [" + probability * 100.0 + "]%");
    }

    /**
     * Method show final results of the second condition : excpected number of guests who will hear the rumor
     *
     * @since 1.0
     */
    public void showAvarageNumberOfPeopleWhoHeared() {
        System.out.println("Expected number of people = [" + Math.ceil(this.averageNumberWhoHeared) + "] people");
    }

    public void increaseNumberOfPeopleWhoHeared() {
        this.numberOfPeopleFromAllAttempts++;
    }


    /**
     * Check our ArrayList to know is all guests hear the rumor
     *
     * @param guests the amount of guests who heared
     * @return true if the guests contains all guests
     * @since 1.0
     */
    public boolean isAllHeared(ArrayList<Integer> guests) {
        return guests.size() == n ? true : false;
    }

    /**
     * Default method to return probability in percent
     *
     * @return probability in percent
     * @since 1.0
     */
    public double getProbabilityInPercent() {
        return this.probability * 100;
    }


    /**
     * Default method to return probability
     *
     * @return probability
     * @since 1.0
     */
    public float getProbability() {
        return this.probability;
    }


    /**
     * Default method to return number of guests from all attempts
     *
     * @return number of guests from all attempts
     * @since 1.0
     */
    public int getNumberOfPeopleFromAllAttempts() {
        return numberOfPeopleFromAllAttempts;
    }


}
