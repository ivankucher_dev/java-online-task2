package com.epam.training;

/**
 * Random class using for {@link com.epam.training.RumorProbabilityEstimator}
 * to get random guest from guests list
 *
 * @author Ivan Kucher
 * @version 1.0
 * @since 2019-07-30
 */
public class Random {

    /**
     * Method realize simple random function random() from {@link Math}
     *
     * @param n        the number of guests
     * @param previous flag to identify previous guest , from whom current guest
     *                 heared the rumor
     * @param current  flag to identify current guest , from whom we randomize
     * @return random guest
     * @since 1.0
     */
    public static int getRandom(int n, int previous, int current) {
        int result = (int) (Math.random() * (n));
        while (result == previous || result == current) {
            result = (int) (Math.random() * (n));

        }


        return result;
    }

}
